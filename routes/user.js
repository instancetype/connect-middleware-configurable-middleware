/**
 * Created by instancetype on 6/9/14.
 */
var routes = {
    GET: {
        '/users': function(req, res) {
            res.end('Jitsu, Kalymba');
        },
        '/users/:id': function(req, res, id) {
            res.end('Showing user ' + id);
        }
    },

    DELETE: {
        '/user/:id': function(req, res, id) {
            res.end('Deleted user ' + id);
        }
    }
};

module.exports = routes;