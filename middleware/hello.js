/**
 * Created by instancetype on 6/9/14.
 */
function hello(req, res) {
    res.writeHead('Content-Type', 'text/plain');
    res.end('Hello\n');
}

module.exports = hello;