/**
 * Created by instancetype on 6/9/14.
 */
var connect = require('connect'),
    logger = require('./middleware/configurable_logger'),
    router = require('./middleware/router'),
    user = require('./routes/user'),
    hello = require('./middleware/hello');


var app = connect()
    .use(logger(':method :url'))
    .use(router(user))
    .use(hello)
    .listen(3000);



